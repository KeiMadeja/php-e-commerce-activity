<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | All orders";
    };

    function getContent(){
        // We use require when we need to interact with data from the database.
        require "controllers/connection.php";
?>    
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <h1 class="text-center py-3">All orders</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Order ID:</th>
                            <th>User ID:</th>
                            <th>Username:</th>
                            <th>Order Date:</th>
                            <th>Items and Quantity:</th>
                            <th>Total:</th>
                            <th>Status:</th>
                            <th>Payment Method:</th>
                            <th>Cancel Button:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $all_orders_query = "SELECT orders.id AS orderId, users.id AS userID, users.firstName AS userFirstName, users.lastName AS userLastName, orders.orderDate AS orderDate, orders.total AS orderTotal, statuses.name AS status, payments.name AS payment FROM orders JOIN users ON (orders.user_id = users.id) JOIN payments ON (orders.payment_id = payments.id) JOIN statuses ON (orders.status_id = statuses.id) ORDER BY orders.id";

                        $allOrders = mysqli_query($conn, $all_orders_query);

                            foreach($allOrders as $indivOrder){
                        ?>
                            <td><?php echo $indivOrder['orderId']; ?></td>
                            <td><?php echo $indivOrder['userID']; ?></td>
                            <td><?php echo $indivOrder['userFirstName'] . " " . $indivOrder['userLastName']; ?></td>
                            <td><?php echo $indivOrder['orderDate']; ?></td>
                            <td>
                            <?php
                                $orderId = $indivOrder['orderId'];

                                $items_query = "SELECT items.name AS item_name, item_order.quantity AS quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $orderId";

                                $items = mysqli_query($conn, $items_query);

                                foreach($items as $indivItem){
                            ?>
                                <span><?php echo $indivItem['quantity'] . " - " .$indivItem['item_name']?></span><br>
                            <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $indivOrder['orderTotal']; ?></td>
                            <td><?php echo $indivOrder['status']; ?></td>
                            <td><?php echo $indivOrder['payment']; ?></td>
                            <td>
                            <?php
                                if($indivOrder['status'] !== "paid"){
                            ?>
                                <a href="controllers/process_cancel_order.php?order_id=<?php echo $indivOrder['orderId'] ?>" class="btn btn-danger">Cancel</a>
                            <?php        
                                }
                            ?>
                            <?php
                                if($indivOrder['status'] !== "cancelled"){
                            ?>
                                <a href="controllers/process_mark_as_delivered.php?order_id=<?php echo $indivOrder['orderId']; ?>" class="btn btn-primary">Mark as Delivered</a>
                            <?php
                                }
                            ?>
                            </td>
                        </tr>
                        <?php
                            }           
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php   
    }   
?>