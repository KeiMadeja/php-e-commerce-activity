<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Add Profile";
    };

    function getContent(){
?>

    <div class="d-flex flex-column justify-content-center align-items-center vh-100">    
        <h1 class="text-center py-5">Add Profile Details:</h1>
        <form action="controllers/process_add_profile.php" method="POST" class="mb-5" enctype="multipart/form-data">
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" name="address" class="form-control" placeholder="House Number, Street, Barangay, City">
            </div>
            <div class="form-group">
                <label for="contactNo">Contact No.:</label>
                <input type="text" name="contactNo" class="form-control" placeholder="+639XX XXX XXXX">
            </div>
            <div class="form-group">
                <label for="profileImg">Profile picture:</label>
                <input type="file" name="profileImg" class="form-control">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-info">Add details</button>
            </div>
        </form>
    </div>

<?php
    }
?>