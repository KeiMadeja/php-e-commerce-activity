<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Cart";
    }

    function getContent(){
        require "controllers/connection.php"
?>
<h1 class="text-center py-5">Cart Page</h1>
<hr>

<div class="table-responsive col-lg-10 offset-lg-1">
    <table class="table table-striped table-bordered">
        <thead>
            <tr class="text-center">
                <th>Item</th>                
                <th>Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                // we need to get all the items from the session
                $total = 0;
                if(isset($_SESSION['cart'])){
                    foreach($_SESSION['cart'] as $item_id => $quantity){
                    // we need to get the details of the item (name, price)
                    // we need to check the data from our db
                    $item_query = "SELECT * FROM items WHERE id = $item_id";
                    
                    $item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

                    $subtotal = $quantity * $item['price'];

                    $total += $subtotal

            ?>
                <tr>
                    <td><?php echo $item['name']; ?></td>
                    <td><?php echo $item['price']; ?></td>
                    <td><?php echo $quantity; ?></td>
                    <td><?php echo $subtotal; ?></td>
                    <td>
                        <a href="controllers/process_remove_item_from_cart.php?item_id=<?php echo $item['id']; ?>" class="btn btn-danger">Remove Item</a>
                    </td>

                </tr>
            <?php
                    }
                }
                // we need to get the details of the item (name, price)
                // We need to check the data from our db
                // since we already know the price, we can get the subtotal by multiplying qty * price
                // since we already know the subtotal, we can get the total amount of the cart
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total: <?php echo $total; ?></td>
                <td>
                    <a href="controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <a href="controllers/process_checkout.php" class="btn btn-info">Pay via COD</a>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>

<?php
    }
?>