CREATE DATABASE b67Ecommerce;

CREATE TABLE categories (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE roles(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE statuses(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE payments(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id)
);

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255),
	role_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(role_id)
	REFERENCES roles(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE items(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255),
	description TEXT,
	price INT,
	quantity INT,
	category_id INT,
	imgPath VARCHAR(255),
	PRIMARY KEY (id),
	FOREIGN KEY(category_id)
	REFERENCES categories(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE orders(
	id INT NOT NULL AUTO_INCREMENT,
	total INT,
	orderDate DATE DEFAULT NOW(),
	status_id INT,
	payment_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY(status_id)
	REFERENCES statuses(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY (payment_id)
	REFERENCES payments(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE item_order(
	id INT NOT NULL AUTO_INCREMENT,
	item_id INT,
	order_id INT,
	PRIMARY KEY (id),
	FOREIGN KEY(item_id)
	REFERENCES items(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	FOREIGN KEY (order_id)
	REFERENCES orders(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

INSERT INTO roles(name) VALUES("admin"), ("customer");

INSERT INTO statuses(name) VALUES("pending"), ("paid"), ("cancelled");

INSERT INTO payments(name) VALUES("cash"), ("deposit"), ("paypal");

INSERT INTO categories(name) VALUES("facialCleansers"), ("moisturizers"), ("toners");

INSERT INTO items(name, description, price, quantity, category_id, imgPath) VALUES
("CosRXLow pH Good Morning Gel Cleanser", "A gentle gel type cleanser with midly acidic pH level", 1000, 1, 1, "randomString"),
("Hada Labo Tokyo Hydrating Facial Cleanser", "A hydrating, gentle foaming, creamy cleanser which purifies skin while protecting and locking in skin's essential moisture, leaving it clean, refreshed, silky smooth and healthy looking.", 1000, 1, 1, "randomString");