<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Add Profile";
    };

    function getContent(){
        require "controllers/connection.php";

        $userId = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles WHERE user_id = $userId";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
?>

    <div class="d-flex flex-column justify-content-center align-items-center vh-100">    
        <h1 class="text-center py-5">Update Profile Details:</h1>
        <form action="controllers/process_add_profile.php" method="POST" class="mb-5" enctype="multipart/form-data">
            <div class="form-group">
                <label for="firstName">First Name:</label>
                <input type="text" name="firstName" class="form-control" value="<?php echo $_SESSION['user']['firstName']; ?>">
            </div>
            <div class="form-group">
                <label for="lastName">Last Name:</label>
                <input type="text" name="lastName" class="form-control" value="<?php echo $_SESSION['user']['lastName']; ?>">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" class="form-control" value="<?php echo $_SESSION['user']['email']; ?>">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="text" name="password" class="form-control" value="<?php echo $_SESSION['user']['password']; ?>">
            </div>
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" name="address" class="form-control" value="<?php echo $profile['address']; ?>">
            </div>
            <div class="form-group">
                <label for="contactNo">Contact No.:</label>
                <input type="text" name="contactNo" class="form-control" value="<?php echo $profile['contactNo']; ?>">
            </div>
            <div class="form-group">
                <label for="profileImg">Profile picture:</label>
                <img src="<?php echo $profile['profileImg']; ?>" height="200px" alt="">
                <input type="file" name="profileImg" class="form-control">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-info">Update details</button>
            </div>
        </form>
    </div>
<?php
    }
?>