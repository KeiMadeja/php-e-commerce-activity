<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Profile";
    };

    function getContent(){
        // require used to get data from the database
        require "controllers/connection.php";

        $userId = $_SESSION['user']['id'];
        $profile_query = "SELECT * FROM profiles WHERE user_id = $userId";
        $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));
                
        if(!$profile){
    ?>
        <div class="d-flex flex-column justify-content-center align-items-center text-center vh-100">
            <h1>You need to update your profile</h1>
            <a href="add-profile.php" class="btn btn-info">Add profile</a>
        </div>
    <?php
        }else{
    ?>
        <div class="container my-5">
            <div class="row my-5">
                <div class="col-lg-6 offset-lg-3">
                    <h1 class="text-center">Your Profile</h1>
                    <div class="text-center">
                        <img src="<?php echo $profile['profileImg']; ?>"  height="500px" width="450px" class="rounded-circle my-5" alt="">
                    </div>
                    <table class="table table-striped">
                        <tr>
                            <td>First name:</td> 
                            <td><?php echo $_SESSION['user']['firstName']; ?></td>
                        </tr>
                        <tr>
                            <td>Last name:</td> 
                            <td><?php echo $_SESSION['user']['lastName']; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td> 
                            <td><?php echo $_SESSION['user']['email']; ?></td>
                        </tr>
                        <tr>
                            <td>Address:</td> 
                            <td><?php echo $profile['address']; ?></td>
                        </tr>
                        <tr>
                            <td>Contact No.:</td> 
                            <td><?php echo $profile['contactNo']; ?></td>
                        </tr>
                    </table>
                    <div class="text-center pt-3">
                        <a href="update-profile.php" class="btn btn-info">Update Profile:</a>
                        <a href="" class="btn btn-danger">Delete Profile: </a>
                    </div>
                </div>
            </div>
        </div>
    <?php        
        };    
    ?> 
    
<?php
        };
?>