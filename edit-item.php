<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Edit Item";
    }

    function getContent(){
        require "controllers/connection.php";
        // get the item id
        $itemId = $_GET['item_id'];
        $item_query = "SELECT * FROM items WHERE id = $itemId";
        $item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

?>

<!-- Add form content -->
<h1 class="text-center py-5">Edit Item Form</h1>
    <div class="d-flex justify-content-center align-items-center">
    <!-- !!! If we want to capture data from any input type file, we need to add the attribut enctype = "multipart/form-data" in our form tag -->
        <form action="controllers/process_edit_item.php" method="POST" class="mb-5" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Item Name:</label>
                <input type="text" name="name" class="form-control" value="<?php echo $item['name']; ?>">
            </div>
            <div class="form-group">
                <label for="price">Items Price:</label>
                <input type="number" name="price" class="form-control" value="<?php echo $item['price']; ?>">
            </div>
            <div class="form-group">
                <label for="quantity">Item Quantity:</label>
                <input type="number" name="quantity" class="form-control" value="<?php echo $item['quantity']; ?>">
            </div>
            <div class="form-group">
                <label for="description">Item Description:</label>
                <textarea name="description" class="form-control"><?php echo $item['description']; ?></textarea>
            </div>

            <!-- For the image -->
            <div class="form-group">
                <label for="imgPath">Item Image:</label>
                <img src="<?php echo $item['imgPath']; ?>" height="50px" width="50px" alt="">
                <input type="file" name="imgPath" class="form-control">
            </div>
            
            <!-- For the category -->
            <div class="form-group">
                <label for="category_id">Category:</label>
                <select name="category_id" class="form-control">
                    <?php
                        require "controllers/connection.php";

                        $category_query = "SELECT * FROM categories";
                        $categories = mysqli_query($conn, $category_query);
                        foreach($categories as $indivCategory){
                            // what we want is to check if indivCategory[id]===item[category_id]
                            // if equal print "selected"
                            // if-else || ternary operator
                    ?>
                    <option value="<?php echo $indivCategory['id']; ?>"

                    <?php echo $indivCategory['id'] === $item['category_id'] ? "selected" : "" ?>

                    >
                    <?php echo $indivCategory['name']?>
                    
                    </option>

                    <?php
                        }
                    ?>
                </select>
            </div>
            <!-- this input is for us to get the id of the item we are editing, this is not meant to be edited by the user. So therefor, we need to hide this -->
            <input type="hidden" name="item_id" value="<?php echo $item['id']; ?>">
            <button class="btn btn-dark my-3" type="submit">Edit Item</button>
        </form>
    </div>

<?php
    }

?>