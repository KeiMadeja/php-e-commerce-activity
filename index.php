<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA";
    }
    function getContent(){
    // in here we want to publish the content of the index.php
    require "controllers/connection.php";
?>
<!-- Catalog Body -->
<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <h3 class="py-2">Categories</h3>
            <ul class="list-group border">
                <li class="list-group-item">
                    <a href="index.php">All</a>
                </li>
                <!-- list of all the items in the category -->
                <?php
                    $category_query = "SELECT * FROM categories";
                    
                    $categories = mysqli_query($conn, $category_query);

                    foreach($categories as $indivCategory){
                ?>
                <li class="list-group-item">
                    <a href="index.php?category_id=<?php echo $indivCategory['id'];?>"><?php echo $indivCategory['name']; ?></a>
                </li>
                <?php        
                    }
                ?>
            </ul>

            <!-- Sorting prices -->
            <h3 class="py-5">Sort By</h3>
            <ul class="list-group border">
                <li class="list-group-item">
                    <a href="controllers/process_sort.php?sort=asc">Price (Lowest to Highest)</a>
                </li>
                <li class="list-group-item">
                    <a href="controllers/process_sort.php?sort=desc">Price (Highest to Lowest)</a>
                </li>
            </ul>
        </div>

        <!-- Catalog cards -->
        <div class="col-lg-10">
            <h1 class="text-center display-3 my-5">Catalog</h1>
            <div class="row">
                <!-- We need to access the db to get the list of items -->
                <?php
                    // We will now get the items from the db
                    // 1. create the query
                    $items_query = "SELECT * FROM items";

                    // Next goal, if we have a category_id in the url, we will filter the items query to only get the items
                    // with category_id = to the value of the url 
                    // $_GET --- this gets the data from the url
                    if(isset($_GET['category_id'])){
                        $categoryId = $_GET['category_id'];
                        // if category_id exists, we will concatenate WHERE category_id = $categoryId to the existing items query
                        $items_query .= " WHERE category_id = $categoryId ";
                    }

                    // this is to add the data from the session if the session exists
                    if(isset($_SESSION['sortDataFromSession'])){
                        $items_query .= $_SESSION['sortDataFromSession'];
                    }


                    // 2. access the db via msqli_query
                    $items = mysqli_query($conn, $items_query);
                    foreach($items as $indivItem){
                ?>
                    <div class="col-lg-4 py-2">
                        <div class="card">
                            <img class="card-img-top" height="200px" src="<?php echo $indivItem['imgPath']?>">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $indivItem['name']?></h4>

                                <p class="card-text">PHP <?php echo $indivItem['price']?></p>
                                
                                <p class="card-text">Item Description: <?php echo $indivItem['description']?></p>
                                
                                <p class="card-text">Quantity: <?php echo $indivItem['quantity']?></p>
                                <?php
                                // we want to publish the category name.
                                // so far, we only have the category_id.
                                // so what we can do is to look for the category row where id = category_id
                                $categoryId = $indivItem['category_id'];
                                $category_query = "SELECT * FROM categories WHERE id = $categoryId";
                                $category = mysqli_fetch_assoc( mysqli_query($conn, $category_query));
                                // !!!IMPORTANT !!! whenever we are retrieving a single row, we need to transform the result into an associative array for us to be able to use the result
                                ?>
                                <p class="card-text">Category: <?php echo ($category['name']); ?></p>
                            </div>
                            <?php
                                if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] === "1"){
                            ?>
                                <div class="card-footer">
                                <!-- we will refactor this part later on -->
                                <a href="controllers/process_delete_item.php?item_id=<?= $indivItem['id']; ?>" class="btn btn-danger">Delete Item</a>
                                <a href="edit-item.php?item_id=<?= $indivItem['id']; ?>" class="btn btn-info">Edit Item</a>
                                </div>
                            <?php
                                }else{
                            ?>
                                <div class="card-footer">
                                <form action="controllers/process_add_to_cart.php" method="POST">
                                    <input type="number" class="form-control" name="quantity">
                                    <input type="hidden" name="item_id" value="<?php echo $indivItem['id'] ?>">
                                    <input type="hidden" name="quantity_from_db" value="<?php echo $indivItem['quantity'] ?>">
                                    <input type="hidden" name="item_name" value="<?php echo $indivItem['name']; ?>">
                                    <button type="button" class="btn btn-info addToCart">Add to Cart</button>
                                </form>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>

