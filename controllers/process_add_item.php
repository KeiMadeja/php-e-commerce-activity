<?php
    require "connection.php";

    $name = $_POST['name'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    // add "addslashes" para hindi ma-mess up ng apostrophes yung code mo.
    $description = addslashes($_POST['description']);
    $category_id = $_POST['category_id'];

    // to save the uploaded image:
    // set up the destination of the image
    $destination = "../assets/images/";

    // create the file name
    $filename = $_FILES['imgPath']['name'];
    
    // move the image
    move_uploaded_file($_FILES['imgPath']['tmp_name'], $destination.$filename);
    
    // create the imgPath variable that we will save in our database
    $imgPath = $destination.$filename;

    $add_item_query = "INSERT INTO items (name, price, quantity, description, category_id, imgPath) VALUES ('$name', $price, $quantity, '$description', $category_id, '$imgPath')";
    
    $new_item = mysqli_query($conn, $add_item_query);

    // redirect to catalog page
    header("Location: ../index.php");

?>

