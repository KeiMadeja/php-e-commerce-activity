<?php
    
    require "connection.php";
    
    $itemId = $_POST['item_id'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $quantity = $_POST['quantity'];
    $description = $_POST['description'];
    $category_id = $_POST['category_id'];

    $image_query = "SELECT imgPath FROM items WHERE id = $itemId";

    $image_result = mysqli_fetch_assoc (mysqli_query($conn, $image_query));

    $imgPath = "";

    if($_FILES['imgPath']['name'] === ""){
        $imgPath = $image_result['imgPath'];
    }else{
        $destination = "../assets/images/";
       
        $fileName = $_FILES['imgPath']['name'];

        move_uploaded_file($_FILES['imgPath']['tmp_name'], $destination.$fileName);

        $imgPath = $destination.$fileName;
    }

    $edit_item_query = "UPDATE items SET name = '$name', price = $price, quantity = $quantity, description = '$description', category_id = $category_id, imgPath = '$imgPath' WHERE id = $itemId";

    $edited_item = mysqli_query($conn, $edit_item_query);

    header("Location: ../index.php");

?>