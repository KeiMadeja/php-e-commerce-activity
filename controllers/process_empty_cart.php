<?php
    session_start();

    // to reset the value of the variable. tinatanggal ung laman ng variable na nilagay sa loob or sa parameter
    unset($_SESSION['cart']);

    // redirect to cart page
    header("Location: " . $_SERVER['HTTP_REFERER']);
?>