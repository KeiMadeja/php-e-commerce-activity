<?php
session_start();
// our goal is to modify the query (items query) and add the sorting
    if(isset($_GET['sort'])){
        if($_GET['sort'] === 'asc'){
           $_SESSION['sortDataFromSession'] = " ORDER BY price ASC";
        }else{
            $_SESSION['sortDataFromSession'] =  " ORDER BY price DESC";
        }
    }

    // this is how to redirect if we know the path
    // header("Location: ../index.php");

    // if we will redirect to the page where we came from. Ibabalik ka lang nya dun sa page kung saan ka nanggaling.
    header("Location: " . $_SERVER['HTTP_REFERER'])
?>

<!-- $_SESSION -->
<!-- Stores information so multiple pages can share information -->
<!-- WHENEVER WE WILL USE $_SESSION, WE NEED TO START THE SESSION -->
<!-- ung session start nasa template.php -->
