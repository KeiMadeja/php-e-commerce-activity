<?php
    // the goal is to remove an item from the variable $_SESSION['cart'];
    // We need an identifier to know which item we will remove

    session_start();

    unset($_SESSION['cart'][$item_id]);

    header("Location: " . $_SERVER['HTTP_REFERER']);

?>