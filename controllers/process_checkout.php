<?php 
	session_start();
	require "connection.php";

	//intialize the orders data
	$total = 0; // we'll recompute this later;
	$status_id = 1; //pending
	$payment_id = 1; //cash
	$user_id = $_SESSION['user']['id'];

	//save it to our orders table;
	$order_query = "INSERT INTO orders (total, status_id, payment_id, user_id) VALUES ($total, $status_id, $payment_id, $user_id)";

	//don't worry about the total, before we end this process we will update the total 
	
	$result = mysqli_query($conn, $order_query);

	//remember, we need to get the id of the order we just saved.
	$order_id = mysqli_insert_id($conn);

	//we'll get the item_id from the session. AND AT THE SAME TIME, we will compute for the total
	foreach($_SESSION['cart'] as $item_id => $quantity){
		//but, in order for us to compute the total, we also need to know the price of the item.
		$item_query = "SELECT price FROM items WHERE id = $item_id";
		//this will result in an associative array named price with a property called price.
		$price = mysqli_fetch_assoc(mysqli_query($conn, $item_query));

		$subtotal = $price['price'] * $quantity;

		//update the total 
		$total += $subtotal;

		//save the item_id and order_id in our table
		$item_order_query = "INSERT INTO item_order (item_id, order_id, quantity) VALUES ($item_id, $order_id, $quantity)";

		$result = mysqli_query($conn, $item_order_query);
	}
	
	//we need to update the total of the order in the db;
	$update_total = "UPDATE orders SET total = $total WHERE id = $order_id";

	$update_result = mysqli_query($conn, $update_total);

	//remove the session or the items in cart and start fresh
	unset($_SESSION['cart']);

// 	1. Upon checking our orders table, we need to insert the following data:
// 	a. total --- we'll compute this
// 	b. status_id --- upon checking the statuses table, we'll use 1 (pending);
// 	c. payment_id --- upon checking the payments table, and since the button says COD, we'll use 1 (Cash);
// 	d. user_id --- we can get this from the session;

// 2. But we noticed, we also have an item_order table;
// this table has the list of items in an order.

// 3. So, before we can update our item_order, we need to save first the order to get the order_id;

// 4. But we need to also save the individual item_id(s) of the order. We can get the item_ids from the SESSION['cart'];

header("Location: ../order-history.php");

 ?>

