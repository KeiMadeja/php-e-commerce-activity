<?php
    session_start();
    // capture the data we need
    // capture the id of the item
    $item_id = $_POST['item_id'];
    // quantity of the item
    $quantity_from_form = $_POST['quantity'];

    $quantity_from_db = $_POST['quantity_from_db'];

    // we need to check if the quantity added to cart is <= the actual quantity in our db
    // if not enough, error
    // if enough, proceed
    if($quantity_from_form > $quantity_from_db){
        die('Insufficient');
    }else{
        if(isset($_SESSION['cart'][$item_id])){
            if($_SESSION['cart'][$item_id] + $quantity_from_form > $quantity_from_db){
                die("Insufficient stocks");
            }else{
            $_SESSION['cart'][$item_id] += $quantity_from_form;
            }
        }else{
            $_SESSION['cart'][$item_id] = $quantity_from_form;
        }
    }

    // save the item in our session
    // if this is the first time to add an item, create the session and assign the quantity received to the session variable
    // if not, we only need to add the quantity to the existing quantity in our session variable
?>