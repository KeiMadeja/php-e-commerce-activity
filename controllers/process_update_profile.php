<?php
    require "controllers/connnection.php";
    session_start();

    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $address = $_POST['address'];
    $contactNo = $_POST['contactNo'];

    $userId = $_SESSION['user']['id'];

    // for the users table
    $update_user_query = "UPDATE users SET firstName = '$firstName', lastName = '$lastName', email = '$email', password = '$password' WHERE id = $userId";

    $updatedUser = mysqli_query($conn, $update_user_query);

    // para i-set kung sinong user yung nag-update ng profile
    $user_query = "SELECT * FROM users WHERE id = $userId";

    // gumamit ng fetch assoc dahil si user lang naman ang kinukuha
    $user = mysqli_fetch_assoc(mysqli_query($conn, $user_query));

    // probably because nag-update si user, malamang iuupdate din si user sa $_SESSION. I guess???????? 
    unset($_SESSION['user']);
    $_SESSION['user'] = $user;

    // to get the profile img
    $profile_img_query = "SELECT profileImg FROM profiles WHERE user_id = $userId";
    
    $result = mysqli_fetch_assoc(mysqli_query($conn, $profile_img_query));

    $profileImg = "";

    if($_FILES['profileImg']['name'] === ""){
        $profileImg = $result['profileImg'];
    }else{
        $destination = "../assets/images/profileImages/";
        $fileName = $_FILES['profileImg']['name'];
        move_uploaded_file($_FILES['profileImg']['tmp_name'], $destination.$fileName);
        $profileImg = $destination.$fileName;
    }

    $update_profile_query = "UPDATE profiles SET address = '$address', contactNo = '$contactNo', profileImg = '$profileImg' WHERE user_id = $userId";

    $updatedProfile = mysqli_query($conn, $update_profile_query);

    header("Location: ../profile.php");

?>