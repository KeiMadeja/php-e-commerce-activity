<?php
    $host = "localhost"; // this is our current host
    $db_username = "root"; // this is the username of our db
    $db_password = ""; //password for the host
    $db_name = "b67Ecommerce"; //name of the database

    // create the connection
    $conn = mysqli_connect($host, $db_username, $db_password, $db_name);

    // check if the connection is successful
    if(!$conn){
        die("Connection failed" . mysqli_error($conn));
    };
?>