<!-- Activity:

Publish the order data of the currently logged in user.

In this table, you need to publish the order details:

Columns:
Order Id - Order Date - Items with Quantity - Total - Status - Payment Method -- Cancel Button

Challenge 1 - Publish Items with Quantity from item_order using JOIN
Challenge 2 - Instead of publishing status_id as a number, publish it as the actual status
Challenge 3 - Same as challenge 2, but with payment_id

The cancel button is just a button, no need to make it work for now.-->

<?php
    require "template/template.php";

    function getTitle(){
        echo "P(U)ROPAGANDA | Order History";
    };

    function getContent(){
        // We use require when we need to interact with data from the database.
        require "controllers/connection.php";

        // First line, is to choose the object from the Session.
        // Second line is to write the query that we need.
        // Third line applies the query to the database. (Still need to understand difference of mysqli_query and mysqli_fetch_assoc)
        // $user_id = $_SESSION['user']['id'];
        // $profile_query = "SELECT * FROM profiles WHERE user_id = $user_id";
        // $profile = mysqli_fetch_assoc(mysqli_query($conn, $profile_query));

        // $testQuery = "SELECT * FROM item_order JOIN orders ON (orders.id = item_order.order_id)";
        // $testResult = mysqli_fetch_assoc(mysqli_query ($conn, $test_query))

        // var_dump($_SESSION['user']);
?>    
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <h1 class="text-center py-3">Your Order History</h1>

                    <table class="table table-striped">

                        <!-- Stretch Goals: To get the First Name, Last Name, and Address, get the data from the session using the same way that you accessed the user IDs. Print them using echo into the table. -->
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Date</th>
                                <th>Order Details</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Payment Method</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $userId = $_SESSION['user']['id'];

                                $orders_query = "SELECT statuses.name AS status, payments.name AS payment, total, orderDate, orders.id AS orderId FROM orders JOIN statuses ON (statuses.id = orders.status_id) JOIN payments ON (payments.id = orders.payment_id) WHERE user_id = $userId ORDER BY orders.id";

                                $orders = mysqli_query($conn, $orders_query);

                                foreach($orders as $indivOrder){
                            ?>
                                <td><?php echo $indivOrder['orderId']; ?></td>
                                <td><?php echo $indivOrder['orderDate']; ?></td>
                                <td>
                                <?php
                                    $orderId = $indivOrder['orderId'];

                                    $items_query = "SELECT items.name AS itemName, item_order.quantity AS quantity FROM item_order JOIN items ON (items.id = item_order.item_id) WHERE order_id = $orderId";

                                    $items = mysqli_query($conn, $items_query);

                                    foreach($items as $indivItem){
                                ?>
                                    <span><?php echo $indivItem['quantity'] . " - " .$indivItem['itemName']?></span><br>
                                <?php
                                    }
                                ?>
                                </td>
                                <td><?php echo $indivOrder['total']; ?></td>
                                <td><?php echo $indivOrder['status']; ?></td>
                                <td><?php echo $indivOrder['payment']; ?></td>
                                <td>
                                <?php
                                    if($indivOrder['status'] !== "paid"){
                                ?>
                                    <a href="controllers/process_cancel_order.php?order_id=<?php echo $indivOrder['orderId'];?>" class="btn btn-danger">
                                    Cancel
                                    </a>
                                <?php        
                                    }
                                ?>
                                </td>
                            </tr>
                            <?php
                                }           
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
<?php   
    }   
?>